jQuery(document).ready(function($) {

	/**
	 * Table responsive
	 */
	$('.wiki_table').wrap('<div class="table-responsive"></div>');

	/**
	 * Sidebar
	 */
	$('#sidebar .WikiCustomNav h1').click(function() {
		$(this).next('ul').slideToggle();
	});
	$('#sidebar .WikiCustomNav ul').eq(-1).hide();
	$('#sidebar .WikiCustomNav ul').eq(-2).hide();

});
